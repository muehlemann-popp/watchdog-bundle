<?php

namespace WatchdogBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

class WatchdogExtension extends Extension
{

    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
        $loader->load('services.yml');
        
        $container->setParameter('watchdog.hide_credentials', $config['hide_credentials']);
        $container->setParameter('watchdog.credentials_pattern', $config['credentials_pattern']);
        $container->setParameter('watchdog.credentials_hidden_text', $config['credentials_hidden_text']);
    }

}
