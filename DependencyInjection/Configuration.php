<?php

namespace WatchdogBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{

    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('watchdog', 'array');
        $rootNode
            ->children()
                ->booleanNode('hide_credentials')->defaultTrue()->end()
                ->scalarNode('credentials_pattern')->defaultValue('/password|pwd|credential|ccv|card*/')->end()
                ->scalarNode('credentials_hidden_text')->defaultValue('<data hidden>')->end()
            ->end();

        return $treeBuilder;
    }

}
