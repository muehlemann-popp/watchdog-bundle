<?php

namespace WatchdogBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Exception;

/**
 *
 * @author Alex Oleshkevich
 * @ORM\Entity
 * @ORM\Table(name="wdog_exception_logs")
 */
class RequestExceptionRecord
{

    /**
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     *
     * @var string
     * @ORM\Column(type="text")
     */
    private $message;

    /**
     *
     * @var int
     * @ORM\Column(type="integer")
     */
    private $code;

    /**
     *
     * @var string
     * @ORM\Column(type="string", length=2048)
     */
    private $file;

    /**
     *
     * @var int
     * @ORM\Column(type="integer")
     */
    private $line;

    /**
     *
     * @var string
     * @ORM\Column(type="text")
     */
    private $trace;

    /**
     *
     * @var string
     * @ORM\Column(type="text", name="prev_message", nullable=true)
     */
    private $prevMessage;

    /**
     *
     * @var string
     * @ORM\Column(type="text", name="prev_trace", nullable=true)
     */
    private $prevTrace;

    public function __construct(Exception $exception = null)
    {
        if ($exception) {
            $this->message = $exception->getMessage();
            $this->code = $exception->getCode();
            $this->file = $exception->getFile();
            $this->line = $exception->getLine();
            $this->trace = $exception->getTraceAsString();

            if ($prev = $exception->getPrevious()) {
                $this->prevMessage = $prev->getMessage();
                $this->prevTrace = $prev->getTraceAsString();
            }
        }
    }

    public function getId()
    {
        return $this->id;
    }

    public function getMessage()
    {
        return $this->message;
    }

    public function getCode()
    {
        return $this->code;
    }

    public function getFile()
    {
        return $this->file;
    }

    public function getLine()
    {
        return $this->line;
    }

    public function getTrace()
    {
        return $this->trace;
    }

    public function getPrevMessage()
    {
        return $this->prevMessage;
    }

    public function getPrevTrace()
    {
        return $this->prevTrace;
    }

    public function setMessage($message)
    {
        $this->message = $message;
        return $this;
    }

    public function setCode($code)
    {
        $this->code = $code;
        return $this;
    }

    public function setFile($file)
    {
        $this->file = $file;
        return $this;
    }

    public function setLine($line)
    {
        $this->line = $line;
        return $this;
    }

    public function setTrace($trace)
    {
        $this->trace = $trace;
        return $this;
    }

    public function setPrevMessage($prevMessage)
    {
        $this->prevMessage = $prevMessage;
        return $this;
    }

    public function setPrevTrace($prevTrace)
    {
        $this->prevTrace = $prevTrace;
        return $this;
    }

}
