<?php

namespace WatchdogBundle\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 *
 * @author Alex Oleshkevich
 * @ORM\Entity
 * @ORM\Table(name="wdog_request_logs")
 */
class RequestLog
{

    /**
     * @var int
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     *
     * @var string
     * @ORM\Column(type="string")
     */
    private $method;

    /**
     *
     * @var int
     * @ORM\Column(type="integer", name="status_code")
     */
    private $statusCode;

    /**
     *
     * @var string
     * @ORM\Column(type="string", name="request_url", length=2048)
     */
    private $requestUrl;

    /**
     *
     * @var string
     * @ORM\Column(type="json_array")
     */
    private $payload;

    /**
     *
     * @var array
     * @ORM\Column(type="json_array")
     */
    private $server;

    /**
     *
     * @var DateTime
     * @ORM\Column(type="datetime", name="time_start")
     */
    private $timeStart;

    /**
     *
     * @var DateTime
     * @ORM\Column(type="datetime", name="time_end")
     */
    private $timeEnd;

    /**
     *
     * @var string
     * @ORM\Column(type="string")
     */
    private $ip;

    /**
     *
     * @var string
     * @ORM\Column(type="string", nullable=true, length=2048)
     */
    private $referer;

    /**
     *
     * @var string
     * @ORM\Column(type="string", name="session_id")
     */
    private $sessionId;

    /**
     *
     * @var string
     * @ORM\Column(type="string", name="user_agent")
     */
    private $userAgent;

    /**
     *
     * @var bool
     * @ORM\Column(type="boolean", name="is_ajax")
     */
    private $isAjax;
    
    /**
     *
     * @var RequestExceptionRecord
     * @ORM\OneToOne(targetEntity="RequestExceptionRecord", cascade={"all"})
     * @ORM\JoinColumn(name="exception_record_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $exceptionRecord;

    public function __construct()
    {
        $this->timeStart = new DateTime;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getMethod()
    {
        return $this->method;
    }

    public function getRequestUrl()
    {
        return $this->requestUrl;
    }

    public function getPayload()
    {
        return $this->payload;
    }

    public function getServer()
    {
        return $this->server;
    }

    public function getTimeStart()
    {
        return $this->timeStart;
    }

    public function getTimeEnd()
    {
        return $this->timeEnd;
    }

    public function getIp()
    {
        return $this->ip;
    }

    public function getReferer()
    {
        return $this->referer;
    }

    public function getSessionId()
    {
        return $this->sessionId;
    }

    public function getUserAgent()
    {
        return $this->userAgent;
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function setMethod($method)
    {
        $this->method = $method;
        return $this;
    }

    public function setRequestUrl($requestUrl)
    {
        $this->requestUrl = $requestUrl;
        return $this;
    }

    public function setPayload($payload)
    {
        $this->payload = $payload;
        return $this;
    }

    public function setServer($server)
    {
        $this->server = $server;
        return $this;
    }

    public function setTimeStart(DateTime $timeStart)
    {
        $this->timeStart = $timeStart;
        return $this;
    }

    public function setTimeEnd(DateTime $timeEnd)
    {
        $this->timeEnd = $timeEnd;
        return $this;
    }

    public function setIp($ip)
    {
        $this->ip = $ip;
        return $this;
    }

    public function setReferer($referer)
    {
        $this->referer = $referer;
        return $this;
    }

    public function setSessionId($sessionId)
    {
        $this->sessionId = $sessionId;
        return $this;
    }

    public function setUserAgent($userAgent)
    {
        $this->userAgent = $userAgent;
        return $this;
    }

    public function getIsAjax()
    {
        return $this->isAjax;
    }

    public function setIsAjax($isAjax)
    {
        $this->isAjax = $isAjax;
        return $this;
    }

    public function getStatusCode()
    {
        return $this->statusCode;
    }

    public function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;
        return $this;
    }

    public function setExceptionRecord(RequestExceptionRecord $exceptionRecord)
    {
        $this->exceptionRecord = $exceptionRecord;
    }

}
