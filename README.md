# WatchdogBundle
Log incoming HTTP requests.

# Installation

```bash
composer require mpom/watchdog-bundle
```

Add to your AppKernel.php:
```php
    public function registerBundles()
    {
        $bundles = array(
            // ...
            new WatchdogBundle\WatchdogBundle,
            // ...
        )
    }
```

# Configuration

Available options:

```yaml
# app/config/config.yml
watchdog:
	hide_credentials: true # prevent credentials from being stored
	credentials_pattern: "/password|pwd|credential|ccv|card*/" # pattern to match credential key
	credentials_hidden_text: "<data hidden>" # text to use instead of credential

```




