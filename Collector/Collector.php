<?php

namespace WatchdogBundle\Collector;

use DateTime;
use Doctrine\ORM\EntityManager;
use Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use WatchdogBundle\Entity\RequestExceptionRecord;
use WatchdogBundle\Entity\RequestLog;

/**
 *
 * @author Alex Oleshkevich
 */
class Collector
{

    /**
     *
     * @var Exception
     */
    protected $exception;

    /**
     *
     * @var EntityManager
     */
    protected $entityManager;
    
    /**
     *
     * @var bool
     */
    protected $hideCredentials;
    
    /**
     *
     * @var string
     */
    protected $credentialsPattern;
    
    /**
     *
     * @var string
     */
    protected $credentialsHiddenText;

    public function __construct(EntityManager $entityManager, $hideCredentials = true, $credentialsPattern = '', $credentialsHiddenText = '')
    {
        $this->entityManager = $entityManager;
        $this->hideCredentials = $hideCredentials;
        $this->credentialsPattern = $credentialsPattern;
        $this->credentialsHiddenText = $credentialsHiddenText;
    }

    public function setException(Exception $exception)
    {
        $this->exception = $exception;
        return $this;
    }

    public function collectAndFlush(Request $request, Response $response)
    {
        $payload = $_POST;
        if ($this->hideCredentials) {
            $hiddenText = $this->credentialsHiddenText;
            $credsPattern = $this->credentialsPattern;
            array_walk_recursive($payload, function (&$v, $k) use ($hiddenText, $credsPattern) {
                if (preg_match($credsPattern, $k)) {
                    $v = $hiddenText;
                }
            });
        }
        
        $entity = new RequestLog;
        $entity->setIp($request->getClientIp());
        $entity->setMethod($_SERVER['REQUEST_METHOD']);
        $entity->setPayload($payload);
        $entity->setReferer($request->server->get('HTTP_REFERER'));
        $entity->setRequestUrl($request->getRequestUri());
        $entity->setServer($request->server->all());
        $entity->setSessionId(session_id());
        $entity->setUserAgent($request->server->get('HTTP_USER_AGENT'));
        $entity->setTimeStart(new DateTime(date('c', $request->server->get('REQUEST_TIME'))));
        $entity->setTimeEnd(new DateTime);
        $entity->setIsAjax($request->isXmlHttpRequest());
        $entity->setStatusCode($response->getStatusCode());
        
        if ($this->exception) {
            $entity->setExceptionRecord(new RequestExceptionRecord($this->exception));
        }
        
        if ($this->entityManager->isOpen()) {
            $this->entityManager->persist($entity);
            $this->entityManager->flush($entity);
        }
    }

}
