<?php

namespace WatchdogBundle\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use WatchdogBundle\Collector\Collector;

/**
 *
 * @author Alex Oleshkevich
 */
class RequestEventSubscriber implements EventSubscriberInterface
{

    /**
     *
     * @var CollectorF
     */
    protected $collector;

    public function __construct(Collector $collector)
    {
        $this->collector = $collector;
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::EXCEPTION => 'onException',
            KernelEvents::RESPONSE => 'onTerminate',
        ];
    }

    public function onException(GetResponseForExceptionEvent $event)
    {
        $this->collector->setException($event->getException());
    }

    public function onTerminate(FilterResponseEvent $event)
    {
        $this->collector->collectAndFlush($event->getRequest(), $event->getResponse());
    }

}
